const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss')


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/assets/styles/js').webpackConfig({
    //this has nothing to do with alias, it's called 'cache busting', to force browsers to load your last assets
    output: {
        chunkFilename: 'js/[name].js?id=[chunkhash]'
    },
    //this is about the alias
    resolve: {
        alias: {
            vue$: 'vue/dist/vue.runtime.esm.js',
            // now we can access every file inside /resources/js with @, exemple @/components will target the component folder in resources/js no matter from wher you're calling it.
            '@': path.resolve('resources/js')
        }
    }
})

mix.sass('resources/sass/app.scss', 'public/assets/styles/css').options({
    processCssUrls: false,
    postCss: [tailwindcss('resources/js/tailwind.config.js')]
})
