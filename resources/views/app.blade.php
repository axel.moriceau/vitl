<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link href="{{ mix('assets/styles/css/app.css') }}" rel="stylesheet" />
    <script src="{{ mix('assets/styles/js/app.js') }}" defer></script>
  </head>
  <body>
    <noscript>
        Votre navigateur ne prend pas en charge JavaScript actuellement. Veuillez mettre à jour votre navigateur ou activer
        le javascript s'il est désactivé afin de pouvoir profiter de la plateforme.
    </noscript>
    @inertia
    <script>
        window.$ASSET_URL = '{{ config('app.asset_url') }}'
    </script>
  </body>
</html>
