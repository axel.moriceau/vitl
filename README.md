# VITL -> VUE - INERTIA - TAILWINDCSS - LARAVEL 

## Steps to init the vitl stack:

## Do not forget to run `composer install && yarn install` before your first run !

## You can find a complete package.json file at the end of the file. (Some scripts wont work since the corresponding dependencies aren't in this package.json)

### 1. Create the laravel application:
> `laravel new app-name`  

The app can be run via a web server (XAMP,MAMP,VALET) or by running the following command
> `php artisan serve`   


*All the following commands needs to be executed a the root of your project basicaly `cd app-name` after `laravel new app-name`*  
### 2. Install INERTIAJS

> Server side AND Client side are highly recommended on a vitl stack based project

#### Server side

I. install inertia via composer  
`composer require inertiajs/inertia-laravel`   

II. Setup your root template  
In ressources/views create an `app.blade.php` file and paste the following template:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link href="{{ mix('assets/styles/css/app.css') }}" rel="stylesheet" />
    <script src="{{ mix('assets/styles/js/app.js') }}" defer></script>
  </head>
  <body>
    <noscript>
        Votre navigateur ne prend pas en charge JavaScript actuellement. Veuillez mettre à jour votre navigateur ou activer
        le javascript s'il est désactivé afin de pouvoir profiter de la plateforme.
    </noscript>
    @inertia
    <script>
        window.$ASSET_URL = '{{ config('app.asset_url') }}'
    </script>
  </body>
</html>

```
<br>  

And add this `ASSET_URL="${APP_URL}"` in your `.env` file
<br>

Then in `/public` create `/assets/styles` and inside `/styles` create a `/css` folder and a `js` folder and leave them empty

III. middleware

Setup the inertia middleware with `php artisan inertia:middleware` command and then go to `Kernel.php` in `app/http` find the array named `web` (line 32) and past ` \App\Http\Middleware\HandleInertiaRequests::class`, this middleware should always be the last item of the array.


#### Client side
This will only cover the inertia install for vue 2.

I. install vue and vue for inertia

`yarn add @inertiajs/inertia @inertiajs/inertia-vue @inertiajs/progress vue`  /* or npm i @inertiajs/inertia @inertiajs/progress @inertiajs/inertia-vue vue */
<br>
II. Initialize the application:  
<br>
Go to `/resources/js/app.js` and add the following code inside `app.js`  
<br>

```js 
import { App, plugin } from '@inertiajs/inertia-vue'
import Vue from 'vue'
import { InertiaProgress as progress } from '@inertiajs/progress/src'

progress.init({
  // The delay after which the progress bar will
  // appear during navigation, in milliseconds.
  delay: 250,
  // The color of the progress bar.
  color: '#33a584',
  // Whether to include the default NProgress styles.
  includeCSS: true,
  // Whether the NProgress spinner will be shown.
  showSpinner: false
})
Vue.config.productionTip = false
Vue.use(plugin)
// Vue.mixin({ methods: { route: window.route } })
const el = document.getElementById('app')

new Vue({
  render: (h) =>
    h(App, {
      props: {
        initialPage: JSON.parse(el.dataset.page),
        resolveComponent: (name) => import(`@/Pages/${name}`).then((module) => module.default)
      }
    })
}).$mount(el)

```

Note the `@/Pages` the `@` is an alias that we will configure just after we've created the `/Pages` folder in `ressources/js/`  
Configure the `@` alias:  
In `webpack.mix.js` (at the root of the project) add the following:  
```js
const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/assets/styles/js').webpackConfig({
    //this has nothing to do with alias, it's called 'cache busting', to force browsers to load your last assets
    output: { 
        chunkFilename: 'js/[name].js?id=[chunkhash]'
    },
    //this is about the alias
    resolve: {
        alias: {
            vue$: 'vue/dist/vue.runtime.esm.js',
            // now we can access every file inside /resources/js with @, exemple @/components will target the component folder in resources/js no matter from wher you're calling it.
            '@': path.resolve('resources/js')
        }
    }
})
```

IV. Render our first vue file  
Create your first controller with:  
`php artisan make:controller ControllerNameController --invokable` //invokable is useful if you only want your controler to host one main action function like a render, let's see that.  
<br>

Go inside your freshly created controller: `app/Http/Controllers`, it should only have a function `__invoke` which is empty, so let's render a file.  
Add this `return Inertia::render('index');` to your controller, remember the `/Pages` folder ? Go inside and create `index.vue` inside it, you can call it the name you want as long as you match the name in the render() function of your controller.  
Note that the controller handles path for example `render('homepage/shop/cart/index')` will look (according to our configuration in `app.js`) inside `/Pages/homepage/shop/cart/` and will look for `index.vue` in the `cart` folder.  
<br>
So inside the index.vue let's start with the classic Hello World:

```js
<template>
  <h1>Hello World</h1>
</template>

<script>
export default {

}
</script>

<style>

</style>

```
<br>

And now ?  
Open `routes/web.php` and replace the actual route with `Route::get('/', ControllerNameController::class)->name('index');`  
The `->name('index');` is optional, it's an alias for laravel so you can refer your route by the provided name.  
<br>

At this point your application will not run properly beacause of the `<link>` we've added in the bladefile, you can remove it or add ` "/assets/styles/css/app.css": "/assets/styles/css/app.css"` in your `mix-manifest.json` file.  
If the file doesn't exists run `yarn install && yarn dev` to generate it (the `yarn dev` should throw an error for now due to this missing css in the mix manifest)<br>
Let's correct this.

### 3. Add tailwind !

Run: `yarn add tailwindcss@latest postcss@latest autoprefixer@latest`,<br>
then: `npx tailwindcss init` and move the generated tailwind.config.js inside `ressources/js` (check the [doc](https://tailwindcss.com/docs/installation#create-your-configuration-file) to configure tailwind as you want.)<br>
In `resources/` remove the `css` folder and instead create a `sass` folder and add it a `app.scss` file, fill it with:<br>
```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```  
Your IDE might throw some warning but just ignore them.  
<br>

Finally, go to `webpack.mix.js` again and just add this: <br>
```js
const tailwindcss = require('tailwindcss')

/* The code added earlier is here 
*  mix.js(......)
*/

mix.sass('resources/sass/app.scss', 'public/assets/styles/css').options({
    processCssUrls: false,
    postCss: [tailwindcss('resources/js/tailwind.config.js')]
})

```
<br>
Check if it works correctly:  

Add a tailwind class to our h1 in index.vue like this: `<h1 class="m-24 text-6xl">Hello World</h1>`
This will add a margin in every directions of the h1 and increase the size of the text.  
`composer install`<br><br>
`yarn install`<br><br>
`yarn dev`<br><br>

# Connect your database and run your first migration !  
Go to your `.env` file and change these lines:  

```conf
DB_DATABASE=db_name #create this database on your computer for a local database
DB_USERNAME=root
DB_PASSWORD=
```
<br>
When this is done just run:<br>

`php artisan migrate`

Laravel comes with some default migrations (located in: `database/migrations`). You can now fully use your VITL stack based app !

# Documentations:

[Laravel](https://laravel.com/docs/8.x/installation)  
[Inertia client setup](https://inertiajs.com/client-side-setup)  
[Inertia server setup](https://inertiajs.com/server-side-setup)  
[TailwindCSS](https://tailwindcss.com/docs)  

# Extras: <br>
Instead of `yarn dev` go to your `package.json` file and in the scripts add this: `"watch": "yarn run development -- --watch",`.  
Now you can run `yarn watch` to reload your app when you do some changes !


# The package.json file:  
```json
{
    "private": true,
    "scripts": {
        "dev": "yarn run development",
        "development": "cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --config=node_modules/laravel-mix/setup/webpack.config.js",
        "watch": "yarn run development -- --watch",
        "watch-poll": "yarn run watch -- --watch-poll",
        "hot": "cross-env NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --disable-host-check --config=node_modules/laravel-mix/setup/webpack.config.js",
        "prod": "yarn run production",
        "production": "cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --config=node_modules/laravel-mix/setup/webpack.config.js",
        "lint:js": "yarn run eslint --cache --ext .js,.vue --ignore-path .gitignore ./resources/",
        "lint:style": "yarn run stylelint \"resources/**/*.{vue,css,scss}\" --ignore-path .gitignore",
        "lint": "yarn lint:js && yarn lint:style"
    },
    "devDependencies": {
        "axios": "^0.19",
        "cross-env": "^7.0",
        "laravel-mix": "^5.0.1",
        "lodash": "^4.17.19",
        "resolve-url-loader": "^3.1.0",
        "sass": "^1.32.0",
        "sass-loader": "^8.0.2",
        "vue-template-compiler": "^2.6.12"
    },
    "dependencies": {
        "@inertiajs/inertia": "^0.8.2",
        "@inertiajs/inertia-vue": "^0.5.4",
        "@inertiajs/progress": "^0.2.4",
        "autoprefixer": "^10.1.0",
        "postcss": "^8.2.2",
        "tailwindcss": "^2.0.2",
        "vue": "^2.6.12"
    }
}

```
